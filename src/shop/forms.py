from django.contrib.auth.models import User
from django import forms

from .models import Item, Image


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    email = forms.CharField(max_length=75, required=True)

    class Meta:
        model = User
        fields = ['username', 'email', 'password', 'first_name', 'last_name']


class ItemForm(forms.ModelForm):

    class Meta:
        model = Item
        fields = ('name', 'company', 'category', 'text', 'photo', 'price', 'quantity', 'params')


class ImagesForm(forms.ModelForm):

    class Meta:
        model = Image
        fields = ('img', )
