# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-08 15:52
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0010_remove_order_items'),
    ]

    operations = [
        migrations.RenameField(
            model_name='itemimage',
            old_name='img_id',
            new_name='img',
        ),
        migrations.RenameField(
            model_name='itemimage',
            old_name='item_id',
            new_name='item',
        ),
    ]
