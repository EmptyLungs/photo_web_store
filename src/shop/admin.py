from django.contrib import admin
from django.contrib.postgres.fields import JSONField

from .models import Item
from .models import Image
from jsoneditor.forms import JSONEditor



class ImageInline(admin.TabularInline):
    """
    multiple photo upload
    """
    fieldsets = (
        (
            None,
            {
                'fields': ('img', )
            }
        ),
    )
    model = Image
    extra = 0
    max_num = 5


class ShopAdmin(admin.ModelAdmin):
    inlines = (ImageInline, )
    formfield_overrides = {
        JSONField: {
            'widget': JSONEditor
        },
    }

# Register your models here.
admin.site.register(Item, ShopAdmin)
