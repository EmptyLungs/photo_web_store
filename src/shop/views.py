from django.shortcuts import render, redirect
from django.contrib.auth import login
from django.core.mail import send_mail

from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.views.generic.edit import CreateView
from django.views.generic import UpdateView, ListView
from django.views.generic import TemplateView
from django.views.generic import DeleteView
from django.views.generic import View
from django.views.generic import DetailView
from django.http import HttpResponseRedirect

from .forms import UserForm
from .forms import ItemForm
from shop.models import Item, Order, Image, OrderItem


class MainPage(TemplateView):
    template_name = 'mainpage.html'


class IndexView(ListView):
    template_name = 'index.html'
    paginate_by = 8
    model = Item
    context_object_name = 'items'

    def get_queryset(self):
        company = self.request.GET.get('company')
        if company:
            it = Item.objects.filter(company__iexact=company)
        elif self.request.GET.get('search'):
            it = Item.objects.filter(name__iexact=self.request.GET.get('search'))
        else:
            it = Item.objects.all()
        return it


class UserFormView(View):
    form_class = UserForm
    template_name = 'register.html'

    # blank form
    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form': form})

    # process form data

    def post(self, request):
        form = self.form_class(request.POST)

        if form.is_valid():
            user = form.save(commit=False)
            # cleaned (normalized) data
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user.set_password(password)
            user.save()
            # создание пустой корзины вместе с юзером_____________________________
            Order.objects.create(status=False, user=user, total_price=0)

            # user.authenticate(username=username, password=password)

            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('/')
        return render(request, self.template_name, {'form': form})


class AddItemView(PermissionRequiredMixin, CreateView):
    permission_required = 'shop.add_item'
    raise_exception = True
    model = Item
    template_name = 'add.html'
    fields = ['name', 'company', 'category', 'text', 'photo', 'price', 'quantity']
    # form_class = ItemForm
    success_url = '/'


class DeleteItemView(PermissionRequiredMixin, DeleteView):
    model = Item
    template_name = 'item_check_delete.html'
    success_url = '/'
    permission_required = 'shop.delete_item'
    raise_exception = True


class EditItemView(PermissionRequiredMixin, UpdateView):
    permission_required = 'shop.edit_item'
    raise_exception = True
    model = Item
    form_class = ItemForm
    template_name = 'edit.html'
    success_url = '/'


class ItemDetailView(DetailView):
    model = Item
    template_name = 'item.html'
    slug_field = 'pk'

    def get_context_data(self, **kwargs):
        context = super(ItemDetailView, self).get_context_data(**kwargs)
        images = Image.objects.filter(item=self.kwargs['pk'])
        context.update({
            'images': images
        })
        return context


# def addToCart(request, item_id):
class AddToCart(LoginRequiredMixin, View):
    def get(self, request, item_id):
        user = request.user
        item = Item.objects.get(id=item_id)

        if not Order.objects.get(status=False, user=user):
            Order.objects.create(status=False, user=user, total_price=0)

        order = Order.objects.get(status=False, user=user)
        OrderItem.objects.create(order=order, item=item)
        order.total_price += item.price
        order.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


# def removeFromCart(request, item_id):
class RemoveFromCart(LoginRequiredMixin, View):
    def get(self, request, item_id):
        user = request.user
        order = Order.objects.get(user=user, status=False)
        item = Item.objects.get(id=item_id)
        OrderItem.objects.get(order=order, item=item).delete()
        order.total_price -= item.price
        order.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class CartView(LoginRequiredMixin, TemplateView):
    template_name = 'cart.html'

    def get_context_data(self, **kwargs):
        context = super(CartView, self).get_context_data(**kwargs)
        user = self.request.user
        cart = Order.objects.get(user=user, status=False)
        items = OrderItem.objects.filter(order=cart)
        list = []
        for item in items:
            list.append(item.item)
        context.update({
            'cart': list,
            'total_price': cart.total_price
        })
        return context


class Checkout(LoginRequiredMixin, View):
    def get(self, request):
        user = request.user
        order = Order.objects.get(status=False, user=user)
        for item in OrderItem.objects.filter(order=order):
            item.item.quantity -= 1
            item.item.save()
        order.status = True
        order.save()
        Order.objects.create(status=False, user=user, total_price=0)
        # email
        itemlist = []
        items = OrderItem.objects.filter(order=order)
        for item in items:
            itemlist.append(item.item.name + '')
        send_mail(
            'Order #' + str(order.id),
            'Items:',  # пофиксить список товаров в емейле!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
            self.request.user.email,
            ['dmitryrint@gmail.com'],
            fail_silently=False
                  )
        return redirect('/')


class ConfirmCheckout(LoginRequiredMixin, TemplateView):
    template_name = 'checkout.html'


class History(LoginRequiredMixin, TemplateView):
    template_name = 'order_history.html'

    def get_context_data(self, **kwargs):
        context = super(History, self).get_context_data(**kwargs)
        if Order.objects.filter(user=self.request.user, status=True):

            user = self.request.user
            history = Order.objects.filter(user=user, status=True)
            prices = []
            orders = []
            obj = []
            for order in history:

                prices.append(order.total_price)
                # items = order.items.split()
                items = OrderItem.objects.filter(order=order)
                for item in items:
                    obj.append(item.item)
                orders.append(obj)
                obj = []
            context.update({
                'orders': orders,
                'prices': prices,
            })
            return context
        else:
            context.update({
                'orders': [],
                'prices': []
            })
