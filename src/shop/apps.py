from django.apps import AppConfig


class ShopConfig(AppConfig):
    name = 'shop'
    app_label = __package__
