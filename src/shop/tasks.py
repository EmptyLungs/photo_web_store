from celery import shared_task
from django.conf import settings
from django.core.files import File
from shop.models import Item
import requests
import os
from PIL import Image
from io import BytesIO
from uuid import uuid4


@shared_task
def dothumbnail(imageurl, size):
    with open(imageurl.url, 'rb') as img:
        f = img.read()
        b = bytearray(f)

    with BytesIO(f) as raw:
        with Image.open(raw) as photo:
            photo.thumbnail(size)
            photo_path = os.path.join(
                settings.MEDIA_ROOT,
                'temp',
                str(uuid4()) + '.' + photo.format.lower()
            )
            photo.save(photo_path)
            return photo_path


@shared_task
def update(photo_path, pk):
    try:
        photo = Item.objects.get(pk=pk)
        file = File(open(photo_path), 'rb')
        file.name = os.path.basename(file.name)
        photo.thumbnail = file
        photo.save()
        os.remove(photo_path)
    except photo.DoesNotExist:
        pass


def thumb(img, pk, size):
    chain = dothumbnail(img, size) | update.s(pk)
    chain()
