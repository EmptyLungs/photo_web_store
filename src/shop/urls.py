from django.conf.urls import url
from . import views
from django.contrib.auth.views import logout, login
from django.conf.urls.static import static
from django.conf import settings


app_name = 'shop'
urlpatterns = [
    url(r'^$', views.MainPage.as_view(), name='mainpage'),
    url(r'^shop/$', views.IndexView.as_view(), name='index'),
    url(r'^register/', views.UserFormView.as_view(), name='register'),
    url(r'^logout/', logout, {'next_page': '/'}, name='logout'),
    url(r'^login/', login, {'template_name': 'login.html'}, name='login'),
    url(r'^add/', views.AddItemView.as_view(), name='add'),
    url(r'^delete/(?P<pk>\d+)/$', views.DeleteItemView.as_view(), name='delete'),
    url(r'^edit/(?P<pk>[0-9]+)/$', views.EditItemView.as_view(), name='edit'),
    url(r'^shop/item/(?P<pk>[0-9]+)/$', views.ItemDetailView.as_view(), name='detail'),
    url(r'^shop/buy/(\d+)/', views.AddToCart.as_view(), name='add_to_cart'),
    url(r'^cart/remove/(\d+)$', views.RemoveFromCart.as_view(), name='remove_from_cart'),
    url(r'^cart/', views.CartView.as_view(), name='cart'),
    url(r'^checkout', views.Checkout.as_view(), name='checkout'),
    url(r'^confirm_checkout', views.ConfirmCheckout.as_view(), name='confirm_checkout'),
    url(r'^history/', views.History.as_view(), name='history')

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
