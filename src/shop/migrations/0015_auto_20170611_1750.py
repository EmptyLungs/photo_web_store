# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-11 17:50
from __future__ import unicode_literals

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0014_item_params'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='params',
            field=django.contrib.postgres.fields.jsonb.JSONField(default={'Батарея': '', 'Матрица': '', 'ФФ': '', 'Фокусное расстояние': ''}),
        ),
    ]
