from django.contrib.postgres.fields import JSONField
from django.db import models
from django.conf import settings


class Item(models.Model):
    name = models.TextField(
        'Название товара'
    )

    company = models.TextField(
        'Производитель'
    )
    #   фото / видео / аксессуары
    category = models.TextField(
        'Категория'
    )
    photo = models.ImageField(
        blank=True,
        null=True,
        upload_to='photos/%Y/%m'
    )

    price = models.PositiveIntegerField(
        'Цена'
    )
    quantity = models.PositiveIntegerField(
        'Количество'
    )
    text = models.TextField('Описание товара', blank=True, null=True)

    params = JSONField(default={
        'Матрица': '',
        'Фокусное расстояние': '',
        'ФФ': '',
        'Батарея': ''
    })

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'
        app_label = 'shop'


class Order(models.Model):
    status = models.BooleanField('статус', default=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1, on_delete=models.CASCADE)
    total_price = models.PositiveIntegerField('общая цена')


class Image(models.Model):
    img = models.ImageField(blank=True, null=True, upload_to='photos/other/%Y/%m')
    item = models.ForeignKey(Item, default=1, on_delete=models.CASCADE)


class OrderItem(models.Model):
    item = models.ForeignKey(Item, default=1, on_delete=models.CASCADE)
    order = models.ForeignKey(Order, default=1, on_delete=models.CASCADE)

