from shop.models import Order, OrderItem


def cartprice(request):
    if request.user.is_authenticated:
        price = Order.objects.get(status=False, user=request.user).total_price
        return{
            'price_tag': price
        }
    else:
        return {
            'price_tag': 0
        }


def getcartitems(request):
    if request.user.is_authenticated:
        order = Order.objects.get(status=False, user=request.user)
        orderitems = OrderItem.objects.filter(order=order)
        items = []
        for item in orderitems:
            items.append(item.item)
        return{
            'items_tag': items
        }
    else:
        return{
            'items_tag': []
        }
